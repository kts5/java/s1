package com.zuitt.example;

import java.util.Scanner;

public class UserInfo {
    public static void main(String[] args){

        Scanner scan = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = scan.nextLine();

        System.out.println("Last Name:");
        String lastName = scan.nextLine();

        System.out.println("First Subject Grade:");
        double firstSubject = scan.nextDouble();

        System.out.println("Second Subject Grade:");
        double secondSubject = scan.nextDouble();

        System.out.println("Third Subject Grade:");
        double thirdSubject = scan.nextDouble();

        int aveGrade = (int)(firstSubject + secondSubject + thirdSubject) / 3;

        System.out.println("Good day, " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + aveGrade);
    }
}
